import * as proxmoxve from '@muhlba91/pulumi-proxmoxve'
import { remote, types } from '@pulumi/command'
import { ComponentResource, ComponentResourceOptions } from '@pulumi/pulumi'
import * as pulumi from '@pulumi/pulumi'
import { Lease, Gateway } from '../router/lease'

const org = pulumi.getOrganization()
const project = pulumi.getProject()
const stack = pulumi.getStack()

export interface ProxmoxContainerArgs {
  template: {
    id: number
    tempCloneId: number
    ip: string
    disk?: {
      datastoreId?: string
      size?: number
    }
  }
  pve: {
    source: {
      name: string
      connection: types.input.remote.ConnectionArgs
    }
    target?: {
      name: string
      connection: types.input.remote.ConnectionArgs
    }
    datastoreId: string
  }
  disk?: {
    datastoreId?: string
    size?: number
  }
  networkInterface: {
    bridge?: pulumi.Input<string>
    enabled?: pulumi.Input<boolean>
    firewall?: pulumi.Input<boolean>
    macAddress?: pulumi.Input<string>
    mtu?: pulumi.Input<number>
    name: pulumi.Input<string>
    rateLimit?: pulumi.Input<number>
    vlanId?: pulumi.Input<number>
  }
  cpuCores: number
  memory: number
  provisionScript: string
  gateway: pulumi.Input<Gateway>
}

export class ProxmoxContainer extends ComponentResource {
  ct: proxmoxve.ct.Container
  resizeClone?: remote.Command
  cleanup?: remote.Command
  ip: remote.Command
  provision: remote.Command
  lease: Lease
  gateway: pulumi.Input<Gateway>

  constructor(
    name: string,
    args: ProxmoxContainerArgs,
    opts: ComponentResourceOptions
  ) {
    super('home-cloud:ct:ProxmoxContainer', name, args, opts)

    if (!opts?.provider) {
      throw new Error('must provide proxmox provider in options')
    }

    const containerName = `${org}-${stack}-${project}-${name}`

    const {
      template,
      pve: { source, datastoreId },
      disk,
      networkInterface,
      cpuCores,
      memory,
      provisionScript,
      gateway
    } = args

    // Assume target pve node is the same as the source pve node with the template, when undefined
    const target = args.pve.target ? args.pve.target : source
    const tempCloneId = template.tempCloneId

    this.gateway = gateway

    const size = (disk?.size ?? template.disk?.size) || 32

    // Create and resize the temporary clone
    this.resizeClone = new remote.Command(
      `${containerName}-preclone`,
      {
        connection: source.connection,
        create: pulumi.interpolate`
        # Check for non-existence of preclone
        while pct list | grep -q "^${tempCloneId} "; do
          echo "Temporary clone ID ${tempCloneId} is in use. Waiting..."
          sleep 5
        done

        # Perform clone and resize
        pct clone ${template.id} ${tempCloneId} --full --storage ${datastoreId} &&
        pct resize ${tempCloneId} rootfs ${size}G
      `
      },
      {
        parent: this,
        dependsOn: this
      }
    )

    this.ct = new proxmoxve.ct.Container(
      containerName,
      {
        nodeName: target.name,
        clone: {
          vmId: tempCloneId,
          datastoreId,
          nodeName: source.name
        },
        networkInterfaces: [networkInterface],
        cpu: {
          cores: cpuCores
        },
        memory: {
          dedicated: memory
        },
        disk: {
          datastoreId,
          size: disk?.size ? disk?.size : 30
        },
        initialization: {
          hostname: containerName,
          dns: {
            domain: 'lan',
            servers: ['192.168.1.1']
          },
          ipConfigs: [
            {
              ipv4: {
                address: 'dhcp'
              }
            }
          ]
        }
      },
      {
        provider: opts.provider,
        parent: this.resizeClone,
        dependsOn: this.resizeClone,
        deleteBeforeReplace: true
      }
    )

    // Cleanup: delete the temporary clone and release the lock file
    this.cleanup = new remote.Command(
      `${containerName}-cleanup-preclone`,
      {
        connection: source.connection,
        create: pulumi.interpolate`pct destroy ${tempCloneId}`
      },
      {
        parent: this.ct,
        dependsOn: [this.ct, this.resizeClone]
      }
    )

    this.ip = new remote.Command(
      `${containerName}-ip`,
      {
        connection: target.connection,
        create: pulumi.interpolate`while :; do ip=$(pct exec ${this.ct.id} -- ip -4 a show eth0 | grep inet | awk '{print $2}' | cut -d'/' -f1) && [[ $ip ]] && break || sleep 3; done; echo $ip`,
        triggers: [this.ct.id]
      },
      {
        parent: this.ct,
        dependsOn: this.ct
      }
    )

    this.provision = new remote.Command(
      `${containerName}-provision`,
      {
        connection: {
          ...source.connection,
          host: pulumi.interpolate`${this.ip.stdout}`
        },
        create: provisionScript,
        triggers: [this.ip.id]
      },
      {
        parent: this.ip,
        dependsOn: this.ip
      }
    )

    this.lease = new Lease(
      containerName,
      {
        gateway: this.gateway
      },
      {
        parent: this.ip,
        dependsOn: this.ip
      }
    )

    this.registerOutputs({
      provision: this.provision,
      lease: this.lease,
      ip: this.ip.stdout,
      ct: this.ct
    })
  }
}
