import * as path from 'path'
import * as pulumi from '@pulumi/pulumi'
import { CreateResult, DiffResult, UpdateResult } from '@pulumi/pulumi/dynamic'
import { NodeSSH } from 'node-ssh'

const dhcpDomainsLuaScriptPath = path.resolve(
  __dirname,
  'scripts/dhcp-domains.lua'
)

const restartDnsmasqAndResolver =
  '/etc/init.d/dnsmasq restart && /etc/init.d/resolver restart'

export interface Gateway {
  host: string
  port?: 22
  username: string
  password: string
}

interface DomainProviderInputs {
  host: string
  ip: string
  gateway: Gateway
}

type DomainProviderOutputs = DomainProviderInputs

interface DhcpDomain {
  name: string | undefined
  ip: string | undefined
  index: string | undefined
}
// This is the path to the remote DHCP Lua script
const remoteDhcpDomainsLuaScriptPath = '/tmp/dhcp-domains.lua'
class DomainProvider implements pulumi.dynamic.ResourceProvider {
  async create(inputs: DomainProviderInputs): Promise<CreateResult> {
    const id = inputs.host

    const ssh = new NodeSSH()

    try {
      await ssh.connect(inputs.gateway)
      await ssh.putFile(
        dhcpDomainsLuaScriptPath,
        remoteDhcpDomainsLuaScriptPath
      )

      const lockfile = '/tmp/pulumi-edit-dhcp-domains-lock'

      // Create a lock file if it doesn't exist and wait if it does
      await ssh.execCommand(
        `while [ -e ${lockfile} ]; do sleep 1; done; touch ${lockfile}`
      )

      const dhcpDomains: DhcpDomain[] = JSON.parse(
        (await ssh.execCommand(`lua ${remoteDhcpDomainsLuaScriptPath}`)).stdout
      )
      console.log('Existing Domains on `create`:', JSON.stringify(dhcpDomains))

      // Log the IPs of DHCP domains
      console.log(
        'DHCP Domains IPs:',
        dhcpDomains.map((domain) => domain.ip)
      )

      // Create a new DHCP domain
      await ssh.execCommand(
        `uci add dhcp domain; uci set dhcp.@domain[-1].ip=${inputs.ip}; uci set dhcp.@domain[-1].name=${inputs.host}; uci commit dhcp`
      )

      // Reload dnsmasq
      await ssh.execCommand(restartDnsmasqAndResolver)

      // Remove the lock file to unlock other DHCP domain change requests
      await ssh.execCommand(`rm ${lockfile}`)

      const outs: DomainProviderOutputs = {
        ...inputs
      }

      return {
        id,
        outs
      }
    } catch (err) {
      console.error('SSH error:', err)
      throw err
    } finally {
      ssh.dispose()
    }
  }

  async update(id: string, news: DomainProviderInputs): Promise<UpdateResult> {
    const ssh = new NodeSSH()

    try {
      await ssh.connect(news.gateway)

      // Retrieve existing DHCP domains
      const dhcpDomains: DhcpDomain[] = JSON.parse(
        (await ssh.execCommand(`lua ${remoteDhcpDomainsLuaScriptPath}`)).stdout
      )
      console.log('Existing Domains on `update`:', JSON.stringify(dhcpDomains))

      // Find the index of the host to be deleted
      const domainToUpdate = dhcpDomains.find((host) => host.name === id)
      if (!domainToUpdate) throw new Error('DHCP Domain not found')
      const indexToUpdate = domainToUpdate.index

      // Delete the DHCP host configuration
      await ssh.execCommand(
        `uci set dhcp.@domain[${indexToUpdate}].ip=${news.ip}; uci commit dhcp`
      )

      // Reload dnsmasq configuration
      await ssh.execCommand(restartDnsmasqAndResolver)

      console.log(`Deleted DHCP domain ${indexToUpdate} with id (MAC) ${id}`)

      const outs: DomainProviderOutputs = {
        ...news
      }

      return {
        id,
        outs
      } as {
        id: string
        outs: DiffResult
      }
    } catch (err) {
      console.error('SSH error:', err)
    } finally {
      ssh.dispose()
    }

    return {}
  }

  async delete(id: string, props: DomainProviderInputs) {
    const ssh = new NodeSSH()

    try {
      await ssh.connect(props.gateway)

      // Retrieve existing DHCP domains
      const dhcpDomains: DhcpDomain[] = JSON.parse(
        (await ssh.execCommand(`lua ${remoteDhcpDomainsLuaScriptPath}`)).stdout
      )
      console.log('Existing Domains on `delete`:', JSON.stringify(dhcpDomains))

      // Find the index of the domain to be deleted
      const domainToDelete = dhcpDomains.find((domain) => domain.name === id)
      if (!domainToDelete) throw new Error('Hostname entry not found')
      const indexToDelete = domainToDelete.index

      // Delete the DHCP host configuration
      await ssh.execCommand(
        `uci delete dhcp.@domain[${indexToDelete}]; uci commit dhcp`
      )

      // Reload dnsmasq configuration
      await ssh.execCommand(restartDnsmasqAndResolver)

      console.log(`Deleted DHCP host ${indexToDelete} with id (MAC) ${id}`)
    } catch (err) {
      console.error('SSH Error:', err)
    } finally {
      ssh.dispose()
    }
  }
}

export interface DomainResourceArgs {
  ip: pulumi.Input<string>
  host: pulumi.Input<string>
  gateway: pulumi.Input<Gateway>
}

export class Domain extends pulumi.dynamic.Resource {
  public readonly address!: pulumi.Output<string>
  public mac!: pulumi.Output<string>
  public host!: pulumi.Output<string>

  constructor(
    name: string,
    args: DomainResourceArgs,
    opts?: pulumi.CustomResourceOptions
  ) {
    super(new DomainProvider(), name, { ...args, address: undefined }, opts)
  }
}
