import * as pulumi from '@pulumi/pulumi'
import { CreateResult } from '@pulumi/pulumi/dynamic'
import { NodeSSH } from 'node-ssh'

export interface Gateway {
  host: string
  port?: 22
  username: string
  password: string
}

interface LeaseProviderInputs {
  host: string
  gateway: Gateway
}

class LeaseProvider implements pulumi.dynamic.ResourceProvider {
  async create(inputs: LeaseProviderInputs): Promise<CreateResult> {
    const id = inputs.host

    const ssh = new NodeSSH()

    try {
      await ssh.connect(inputs.gateway)

      // Check that a dynamic DHCP lease was assigned
      await ssh.execCommand(`cat /tmp/dhcp.leases* | grep ${id}`)

      const outs = {
        ...inputs
      }

      return {
        id,
        outs
      }
    } catch (err) {
      console.error('SSH Error:', err)
      throw err
    } finally {
      ssh.dispose()
    }
  }

  async delete(id: string, props: LeaseProviderInputs) {
    const ssh = new NodeSSH()

    try {
      await ssh.connect(props.gateway)

      // Remove active leases
      await ssh.execCommand(`sed -i '/${id}/d' /tmp/dhcp.leases`)
      await ssh.execCommand(`sed -i '/${id}/d' /tmp/dhcp.leases.dynamic`)

      // Reload dnsmasq configuration
      await ssh.execCommand('/etc/init.d/dnsmasq reload')

      console.log(`Deleted all dhcp leasees for ${id}`)
    } catch (err) {
      console.error('SSH Error:', err)
    } finally {
      ssh.dispose()
    }
  }
}

export interface LeaseResourceArgs {
  gateway: pulumi.Input<Gateway>
}

export class Lease extends pulumi.dynamic.Resource {
  constructor(
    name: string,
    args: LeaseResourceArgs,
    opts?: pulumi.CustomResourceOptions
  ) {
    super(new LeaseProvider(), name, { ...args, host: name }, opts)
  }
}
