local uci = require("luci.model.uci").cursor()
local json = require("luci.json")

local dhcp_domains = {}
local i = 0

uci:foreach("dhcp", "domain", function(s)
    table.insert(dhcp_domains, {
        name = s.name,
        ip = s.ip,
        index = i
    })
    i = i + 1
end)

print(json.encode(dhcp_domains))