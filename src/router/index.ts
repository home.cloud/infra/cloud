import * as pulumi from '@pulumi/pulumi'
import { Lease, Gateway } from './lease'

export interface OpenWRTArgs {
  gateway: Gateway
}

export class OpenWRT extends pulumi.ComponentResource {
  private leases: Lease[] = []

  public gateway: pulumi.Input<Gateway>

  constructor(
    name: string,
    args: OpenWRTArgs,
    opts?: pulumi.ComponentResourceOptions
  ) {
    super('nebula:network:Router', name, args, opts)

    this.gateway = args.gateway

    this.registerOutputs({
      leases: this.leases
    })
  }
}
