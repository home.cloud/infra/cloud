declare module 'eslint-plugin-import' {
  // Define the structure of your plugin's exports here
  export interface Rule {
    // Define the structure of individual rules
  }

  export interface Config {
    // Define the structure of configurations
  }

  export const rules: Record<string, Rule>
  export const configs: Record<string, Config>
}
