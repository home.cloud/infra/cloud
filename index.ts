import * as proxmoxve from '@muhlba91/pulumi-proxmoxve'
import * as pulumi from '@pulumi/pulumi'
import { ProxmoxContainer, ProxmoxContainerArgs } from './src/ct'
import { OpenWRT } from './src/router'
import { Domain } from './src/router/domain'

const config = new pulumi.Config()

const startingVmId = config.require('startingVmId')
const gateway = config.require('gateway')
const routerPassword = config.requireSecret('routerPassword')

const org = pulumi.getOrganization()
//const project = pulumi.getProject()
const stack = pulumi.getStack()

const containerTemplateStack = new pulumi.StackReference(
  `${org}/container-template/${stack}`
)
const containerTemplateStackOutput = pulumi.unsecret(
  containerTemplateStack.getOutput('out')
)

interface Node {
  name: string
  host: string
}

interface Container {
  node?: Node
  datastoreId: string
  size?: number
}

interface Runner {
  id: number
  token: string
}

interface RunnerContainer extends Container {
  deposition: Runner
  ranker: Runner
  home: Runner
  christina: Runner
}

export interface Features {
  registry: Container | boolean
  npm: Container | boolean
  apt: Container | boolean
  gitlabRunners: RunnerContainer | undefined
  jellyfin: Container | boolean
}

const features = config.requireObject<Features>('features')

const dhcpDomainRegistry = stack === 'live' ? 'registry' : `${stack}-registry`

const outputs = pulumi
  .all([containerTemplateStackOutput, routerPassword])
  .apply(([output, routerPassword]) => {
    const out: {
      registry?: ProxmoxContainer
      npm?: ProxmoxContainer
      apt?: ProxmoxContainer
      gitlabRunner?: ProxmoxContainer
      jellyfin?: ProxmoxContainer
    } = {}

    const {
      pveConnection: connection,
      node: { name: name, host },
      template,
      providerArgs
    } = output

    const router = new OpenWRT('router', {
      gateway: {
        host: gateway,
        username: 'root',
        password: routerPassword
      }
    })

    const proxmoxveProvider = new proxmoxve.Provider('proxmoxve', providerArgs)

    const networkInterface = {
      name: 'eth0',
      bridge: 'vmbr0'
    }

    const containerArgs: ProxmoxContainerArgs = {
      template,
      pve: {
        source: {
          name: name,
          connection
        },
        datastoreId: 'local-lvm'
      },
      networkInterface: networkInterface,
      cpuCores: 4,
      memory: 4096,
      gateway: router.gateway,
      provisionScript: `git clone https://gitlab.com/home.cloud/serve/registry.git
                      cd ~/registry
                      docker compose up -d`
    }

    function isContainer(obj: unknown): obj is Container {
      return typeof obj !== 'boolean'
    }

    if (features.registry) {
      if (isContainer(features.registry)) {
        containerArgs.template.tempCloneId = +startingVmId + 1
        containerArgs.pve = {
          ...containerArgs.pve,
          target: {
            name: features.registry?.node?.name || name,
            connection: {
              ...connection,
              host: features.registry?.node?.host || host
            }
          },
          datastoreId: features.registry.datastoreId
        }

        containerArgs.disk = {
          datastoreId:
            features.registry?.datastoreId ?? template.disk.datastoreId,
          size: features.registry?.size ?? template.disk.size
        }
      }
      out.registry = new ProxmoxContainer('registry', containerArgs, {
        parent: router,
        provider: proxmoxveProvider
      })

      new Domain(
        dhcpDomainRegistry,
        {
          gateway: router.gateway,
          ip: out.registry.ip.stdout,
          host: dhcpDomainRegistry
        },
        {
          dependsOn: out.registry
        }
      )
    }

    // Serve/npm setup
    if (features.npm) {
      if (isContainer(features.npm)) {
        // Adjust condition to check for npm if separate
        containerArgs.template.tempCloneId = +startingVmId + 2
        containerArgs.pve = {
          ...containerArgs.pve,
          target: {
            name: features.npm.node?.name || name,
            connection: {
              ...connection,
              host: features.npm.node?.host || host
            }
          },
          datastoreId: features.npm.datastoreId
        }

        containerArgs.disk = {
          datastoreId: features.npm.datastoreId ?? template.disk.datastoreId,
          size: features.npm.size ?? template.disk.size
        }
      }
      out.npm = new ProxmoxContainer(
        'npm',
        {
          ...containerArgs,
          provisionScript: `git clone https://gitlab.com/home.cloud/serve/npm.git
                          cd ~/npm
                          docker compose up -d --build`
        },
        {
          parent: out.registry!,
          provider: proxmoxveProvider
        }
      )

      const dhcpDomainNPM = stack === 'live' ? 'npm' : `${stack}-npm`
      new Domain(
        dhcpDomainNPM,
        {
          gateway: router.gateway,
          ip: out.npm.ip.stdout,
          host: dhcpDomainNPM
        },
        {
          dependsOn: out.npm
        }
      )
    }

    // Serve/apt setup
    if (features.apt) {
      if (isContainer(features.apt)) {
        // Adjust condition to check for apt if separate
        containerArgs.template.tempCloneId = +startingVmId + 3
        containerArgs.pve = {
          ...containerArgs.pve,
          target: {
            name: features.apt.node?.name || name,
            connection: {
              ...connection,
              host: features.apt.node?.host || host
            }
          },
          datastoreId: features.apt.datastoreId
        }

        containerArgs.disk = {
          datastoreId: features.apt.datastoreId ?? template.disk.datastoreId,
          size: features.apt.size ?? template.disk.size
        }
      }
      out.apt = new ProxmoxContainer(
        'apt',
        {
          ...containerArgs,
          provisionScript: `git clone https://gitlab.com/home.cloud/serve/apt.git
                          cd ~/apt
                          docker compose up -d --build`
        },
        {
          parent: out.npm!,
          provider: proxmoxveProvider
        }
      )

      const dhcpDomainAPT = stack === 'live' ? 'apt' : `${stack}-apt`
      new Domain(
        dhcpDomainAPT,
        {
          gateway: router.gateway,
          ip: out.apt.ip.stdout,
          host: dhcpDomainAPT
        },
        {
          dependsOn: out.apt
        }
      )
    }

    if (features.gitlabRunners) {
      if (isContainer(features.gitlabRunners)) {
        containerArgs.template.tempCloneId = +startingVmId + 4
        containerArgs.pve = {
          ...containerArgs.pve,
          target: {
            name: features.gitlabRunners?.node?.name || name,
            connection: {
              ...connection,
              host: features.gitlabRunners?.node?.host || host
            }
          },
          datastoreId: features.gitlabRunners.datastoreId
        }

        containerArgs.disk = {
          datastoreId:
            features.gitlabRunners?.datastoreId ?? template.disk.datastoreId,
          size: features.gitlabRunners?.size ?? template.disk.size
        }
      }

      out.gitlabRunner = new ProxmoxContainer(
        'gitlab-runner',
        {
          ...containerArgs,
          cpuCores: 10,
          memory: 12096,
          provisionScript: `
            # Clone or update the gitlab-runner repository
            if [ -d ~/gitlab-runner ]; then
              cd ~/gitlab-runner && git pull
            else
              git clone https://gitlab.com/home.cloud/serve/gitlab-runner.git ~/gitlab-runner
            fi

            cd ~/gitlab-runner

            # Generate entrypoint for the "deposition" runner
            export TOKEN=${features.gitlabRunners.deposition.token}
            export RUNNER_NAME="deposition"
            cat entrypoint.sh.template | envsubst > deposition.cloud/entrypoint.sh
            chmod +x deposition.cloud/entrypoint.sh

            # Generate entrypoint for the "ranker-dapp" runner
            export TOKEN=${features.gitlabRunners.ranker.token}
            export RUNNER_NAME="ranker-dapp"
            cat entrypoint.sh.template | envsubst > ranker-dapp/entrypoint.sh
            chmod +x ranker-dapp/entrypoint.sh

            # Generate entrypoint for the "christina-science" runner
            export TOKEN=${features.gitlabRunners.christina.token}
            export RUNNER_NAME="christina-science"
            cat entrypoint.sh.template | envsubst > christina.science/entrypoint.sh
            chmod +x christina.science/entrypoint.sh

            # Generate entrypoint for the "home-cloud" runner
            export TOKEN=${features.gitlabRunners.home.token}
            export RUNNER_NAME="home-cloud"
            cat entrypoint.sh.template | envsubst > home.cloud/entrypoint.sh
            chmod +x home.cloud/entrypoint.sh

            # Ensure Docker is configured with insecure registry and mirror settings
            DOCKER_CONFIG=/etc/docker/daemon.json

            # Create an updated Docker configuration
            NEW_CONFIG=$(jq -n '
            {
              "insecure-registries": ["${dhcpDomainRegistry}.lan:5000", "${dhcpDomainRegistry}.lan:5001"],
              "registry-mirrors": ["http://${dhcpDomainRegistry}.lan:5001"]
            }')

            # Check if current Docker config matches the desired settings
            CURRENT_CONFIG=$(sudo cat $DOCKER_CONFIG | jq -S . || echo '{}')
            DESIRED_CONFIG=$(echo $NEW_CONFIG | jq -S .)

            # Only update and restart Docker if the configuration has changed
            if [ "$CURRENT_CONFIG" != "$DESIRED_CONFIG" ]; then
              echo "$NEW_CONFIG" | sudo tee $DOCKER_CONFIG > /dev/null
              sudo systemctl restart docker
            fi

            # Start the Docker containers
            docker compose up -d

          `
        },
        {
          provider: proxmoxveProvider,
          parent: out.apt!
        }
      )
    }

    if (features.jellyfin) {
      if (isContainer(features.jellyfin)) {
        // Adjust condition to check for jellyfin if separate
        containerArgs.template.tempCloneId = +startingVmId + 2
        containerArgs.pve = {
          ...containerArgs.pve,
          target: {
            name: features.jellyfin.node?.name || name,
            connection: {
              ...connection,
              host: features.jellyfin.node?.host || host
            }
          },
          datastoreId: features.jellyfin.datastoreId
        }

        containerArgs.disk = {
          datastoreId:
            features.jellyfin.datastoreId ?? template.disk.datastoreId,
          size: features.jellyfin.size ?? template.disk.size
        }
      }

      out.jellyfin = new ProxmoxContainer(
        'jellyfin',
        {
          ...containerArgs,
          provisionScript: `git clone https://gitlab.com/home.cloud/serve/jellyfin.git
                          cd ~/jellyfin
                          docker compose up -d --build`
        },
        {
          parent: out.registry!,
          provider: proxmoxveProvider
        }
      )

      const dhcpDomainJellyfin =
        stack === 'live' ? 'jellyfin' : `${stack}-jellyfin`
      new Domain(
        dhcpDomainJellyfin,
        {
          gateway: router.gateway,
          ip: out.jellyfin.ip.stdout,
          host: dhcpDomainJellyfin
        },
        {
          dependsOn: out.jellyfin
        }
      )
    }

    return out
  })

export const out = {
  outputs //: pulumi.unsecret(outputs)
}
