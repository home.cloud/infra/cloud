# Home Cloud Infrastructure with Pulumi

## Overview

Use Pulumi to automate the deployment of home cloud infrastructure on Proxmox Virtual Environment.

## Deployments

### `registry`

A docker registry for use inside the lan.

### `gitlab-runner`

For use against gitlab.com

## Usage

1. **Set Configuration**: Use the `pulumi config set` commands to configure the GitLab Runner ID and token.
2. **Deploy**: Run `pulumi up` to deploy the infrastructure.

**Example Configuration Commands:**

First, create [`New group runner`](https://gitlab.com/groups/deposition.cloud/-/runners) and copy the `glrt-SECRET` `runner authentication token`.

```sh
 pulumi config set --path features.gitlabRunners.deposition.token --secret glrt-SECRET
 pulumi config set --path features.gitlabRunners.home.token --secret glrt-SECRET
 pulumi config set --path features.gitlabRunners.ranker.token --secret glrt-SECRET
 pulumi config set --path features.gitlabRunners.christina.token --secret glrt-SECRET
```

## Uncertainties

- Ensure that the secret tokens and sensitive data are handled securely.
- Validate the paths and URLs in the provisioning scripts.

**Future**:

- [ ] Don't recreate CTs if no changes
- [ ] Consider incorporating error handling or logging mechanisms in the provisioning scripts
- [ ] How can this setup be scaled or extended to include additional services in the future?
- [ ] Is there a need for cleanup or rollback mechanisms in case of deployment failures?
