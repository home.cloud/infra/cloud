import path from 'path'
import js from '@eslint/js'
import typescriptEslint from '@typescript-eslint/eslint-plugin'
import tsParser from '@typescript-eslint/parser'
import prettier from 'eslint-config-prettier'
import importPlugin from 'eslint-plugin-import'
import prettierPlugin from 'eslint-plugin-prettier'
import sonarjs from 'eslint-plugin-sonarjs'
import unicorn from 'eslint-plugin-unicorn'
import tseslint from 'typescript-eslint'

export default [
  // JavaScript recommendations
  js.configs.recommended,
  ...tseslint.configs.recommendedTypeCheckedOnly,

  // TypeScript parser and plugin setup
  {
    files: ['**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],
    languageOptions: {
      parser: tsParser,
      parserOptions: {
        project: './tsconfig.json',
        tsconfigRootDir: __dirname
      }
    },
    plugins: {
      '@typescript-eslint': typescriptEslint
    },
    rules: {
      '@typescript-eslint/no-unused-vars': [
        'error',
        { argsIgnorePattern: '^_' }
      ]
    }
  },
  {
    ignores: ['dist/', 'node_modules/']
  },

  // Prettier configuration
  prettier,

  // Prettier plugin to enforce code style
  {
    plugins: {
      prettier: prettierPlugin
    },
    rules: {
      'prettier/prettier': 'error' // Run Prettier as an ESLint rule
    }
  },

  // Import plugin
  {
    plugins: {
      import: importPlugin
    },
    settings: {
      'import/resolver': {
        node: {
          extensions: ['.js', '.jsx', '.ts', '.tsx']
        },
        typescript: {
          project: path.resolve(__dirname, './tsconfig.json')
        }
      }
    },
    rules: {
      'import/order': ['error', { alphabetize: { order: 'asc' } }],
      'import/no-duplicates': 'error',
      'import/no-unresolved': 'error'
    }
  },

  // Unicorn plugin for modern best practices
  {
    plugins: {
      unicorn
    },
    rules: {
      'unicorn/prefer-spread': 'error',
      'unicorn/no-array-for-each': 'error',
      'unicorn/no-null': 'error'
    }
  },

  // SonarJS plugin for code quality
  {
    plugins: {
      sonarjs
    },
    rules: {
      'sonarjs/no-duplicate-string': 'error',
      'sonarjs/no-all-duplicated-branches': 'error',
      'sonarjs/no-collapsible-if': 'error'
    }
  }
]
